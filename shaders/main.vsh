; $MODE = "UniformRegister"
; $UNIFORM_VARS[0].name = "uMVP"
; $UNIFORM_VARS[0].type = "Matrix4x4"
; $UNIFORM_VARS[0].count = 1
; $UNIFORM_VARS[0].offset = 0
; $UNIFORM_VARS[0].block = -1
; $ATTRIB_VARS[0].name = "aColour"
; $ATTRIB_VARS[0].type = "Float3"
; $ATTRIB_VARS[0].location = 0
; $ATTRIB_VARS[1].name = "aPosition"
; $ATTRIB_VARS[1].type = "Float4"
; $ATTRIB_VARS[1].location = 1

; Note to self: these are packed in groups of four here, and *not* packed on
; the PS side. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
; Semantic 0: aColour
; $SPI_VS_OUT_CONFIG.VS_EXPORT_COUNT = 0
; $NUM_SPI_VS_OUT_ID = 1
; $SPI_VS_OUT_ID[0].SEMANTIC_0 = 0

; R0 is sacrificed as temporary
; MVP is in C0,1,2,3

;   Time to multiply out the uMVP matrix (stored in column-major order)
:   Input matrix:
;   | C0.x C1.x C2.x C3.x |
;   | C0.y C1.y C2.y C3.y |
;   | C0.z C1.z C2.z C3.z |
;   | C0.w C1.w C2.w C3.w |
;   The Plan: (xywz are in R2)
;   PV.x | C0.x*x + C1.x*y + C2.x*z + C3.x*w |
;   PV.y | C0.y*x + C1.y*y + C2.y*z + C3.y*w |
;   PV.z | C0.z*x + C1.z*y + C2.z*z + C3.z*w |
;   PV.w | C0.w*x + C1.w*y + C2.w*z + C3.w*w |
;   This happens for pc 01:0-3.


00 CALL_FS NO_BARRIER
01 ALU: ADDR(32) CNT(16)
    0 x: MUL ____, R2.x, C0.x
      y: MUL ____, R2.x, C0.y
      z: MUL ____, R2.x, C0.z
      w: MUL ____, R2.x, C0.w
    1 x: MULADD R0.x, R2.y, C1.x, PV0.x
      y: MULADD R0.y, R2.y, C1.y, PV0.y
      z: MULADD R0.z, R2.y, C1.z, PV0.z
      w: MULADD R0.w, R2.y, C1.w, PV0.w
    2 x: MULADD R0.x, R2.z, C2.x, PV0.x
      y: MULADD R0.y, R2.z, C2.y, PV0.y
      z: MULADD R0.z, R2.z, C2.z, PV0.z
      w: MULADD R0.w, R2.z, C2.w, PV0.w
    3 x: MULADD R6.x, R2.w, C3.x, PV0.x
      y: MULADD R6.y, R2.w, C3.y, PV0.y
      z: MULADD R6.z, R2.w, C3.z, PV0.z
      w: MULADD R6.w, R2.w, C3.w, PV0.w
02 EXP: PARAM0, R1.xyzw
03 EXP_DONE: POS0, R6.xyzw
END_OF_PROGRAM
