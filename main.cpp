#include <stdlib.h>

#include <coreinit/debug.h>
#include <padscore/kpad.h>
#include <padscore/wpad.h>
#include <whb/log.h>
#include <whb/log_udp.h>
#include <whb/proc.h>
#include <whb/gfx.h>
#include <gx2r/buffer.h>
#include <gx2r/draw.h>
#include <gx2/shaders.h>
#include <gx2/draw.h>
#include <gx2/registers.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shaders.h"
#include "gx2util.hpp"

static WHBGfxShaderGroup shader;
#define ARR_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

int main(int argc, char *argv[])
{
    (void)argc; (void)argv;
    WHBProcInit();
    WHBLogUdpInit();

    KPADInit();
    WPADEnableURCC(1); //needed to connect wiimotes

    WHBGfxInit();

/*  Load shader group (compiled in) */
    if (!WHBGfxLoadGFDShaderGroup(&shader, 0, main_shader)) {
        WHBLogPrint("Failed to load shader!");
        WHBGfxShutdown();
        WHBLogUdpDeinit();
        WHBProcShutdown();
        return 1;
    }

/*  Variables to keep track of buffer numbers for each shader */
    int aPosition, aColour, buffer = 0;
/*  Assign aPosition to next free buffer */
    aPosition = buffer++;
    WHBGfxInitShaderAttribute(&shader, "aPosition", aPosition, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32_32_32);
/*  Assign aColour to next free buffer */
    aColour = buffer++;
    WHBGfxInitShaderAttribute(&shader, "aColour", aColour, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32_32);

/*  Init fetch shader to handle attribute vars */
    WHBGfxInitFetchShader(&shader);

/*  Locate MVP shader uniform (uMVP) */
    GX2UniformVar* uMVP = GX2FindUniformReg(shader.vertexShader, "uMVP");
    if (!uMVP) {
        WHBLogPrint("Couldn't find MVP uniform!");
        WHBGfxShutdown();
        WHBLogUdpDeinit();
        WHBProcShutdown();
        return 1;
    }

/*  Create buffer for vertex position shader attribute var (aPosition) */
    GX2RBuffer aPositions = {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER | GX2R_RESOURCE_USAGE_CPU_WRITE),
        .elemSize = sizeof(glm::vec4),
        .elemCount = 8,
    };
    GX2RCreateBuffer(&aPositions);

/*  Fill out vertexes */
    glm::vec4* aPositions_val = (glm::vec4*)GX2RLockBufferEx(&aPositions, (GX2RResourceFlags)0);
    aPositions_val[0] = glm::vec4(-1.0f, -1.0f,  1.0f, 1.0f);
    aPositions_val[1] = glm::vec4( 1.0f, -1.0f,  1.0f, 1.0f);
    aPositions_val[2] = glm::vec4( 1.0f,  1.0f,  1.0f, 1.0f);
    aPositions_val[3] = glm::vec4(-1.0f,  1.0f,  1.0f, 1.0f);
    aPositions_val[4] = glm::vec4(-0.5f, -0.5f, -2.0f, 1.0f);
    aPositions_val[5] = glm::vec4( 0.5f, -0.5f, -2.0f, 1.0f);
    aPositions_val[6] = glm::vec4( 1.0f,  1.0f, -2.0f, 1.0f);
    aPositions_val[7] = glm::vec4(-1.0f,  1.0f, -2.0f, 1.0f);
    GX2RUnlockBufferEx(&aPositions, (GX2RResourceFlags)0);
    aPositions_val = NULL;

/*  Indexes for rendering a cube (pretty neat optimisation) */
    const uint16_t indexes[] = {
        0, 1, 2, 3,
        1, 5, 6, 2,
        7, 6, 5, 4,
        4, 0, 3, 7,
        4, 5, 1, 0,
        3, 2, 6, 7,
    };

/*  Create buffer for vertex colour shader attribute var (aColour) */
    GX2RBuffer aColours = {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER | GX2R_RESOURCE_USAGE_CPU_WRITE),
        .elemSize = sizeof(glm::vec3),
        .elemCount = aPositions.elemCount,
    };
    GX2RCreateBuffer(&aColours);

/*  Fill out vertex colours */
    glm::vec3* aColours_val = (glm::vec3*)GX2RLockBufferEx(&aColours, (GX2RResourceFlags)0);
    aColours_val[0] = glm::vec3( 0.3f,  0.3f,  0.3f);
    aColours_val[1] = glm::vec3( 0.3f,  0.3f,  0.3f);
    aColours_val[2] = glm::vec3( 0.8f,  0.8f,  0.8f);
    aColours_val[3] = glm::vec3( 0.8f,  0.8f,  0.8f);
    aColours_val[4] = glm::vec3( 0.3f,  0.3f,  0.3f);
    aColours_val[5] = glm::vec3( 0.3f,  0.3f,  0.3f);
    aColours_val[6] = glm::vec3( 0.8f,  0.8f,  0.8f);
    aColours_val[7] = glm::vec3( 0.8f,  0.8f,  0.8f);
    GX2RUnlockBufferEx(&aColours, (GX2RResourceFlags)0);
    aColours_val = NULL;

/*  Create a projection and view matrix, camera at (0,0,-10) world coordinates */
    glm::mat4 ProjectionMatrix = glm::perspective(
        glm::radians(30.0f),
        16.0f / 9.0f,
        0.1f, 100.0f
    );
    glm::mat4 ViewMatrix = glm::translate(glm::mat4(1.0f),
        glm::vec3(0.0f, 0.0f, -10.0f)
    );

/*  Main rendering loop */
    while (WHBProcIsRunning()) {
    /*  Read data from first Wii Remote */
        KPADStatus stat;
        int err, num_reads = KPADReadEx(WPAD_CHAN_0, &stat, 1, &err);

    /*  Accelerometer data is the *direction of gravity* as told by a flat, face-up Wii Remote
     *  Convert this to the direction of "up" according to the Wii Remote. */
        glm::vec3 mote_yaxis(0.0f, 1.0f, 0.0f);
        glm::vec3 chuk_yaxis(0.0f, 1.0f, 0.0f);
        if (err == KPAD_ERROR_OK && num_reads == 1) {
            //WHBLogPrintf("buttons: %08X, %08X\n", stat.hold, stat.nunchuck.hold);

        /*  Negatives convert between down (gravity) and up vectors */
            mote_yaxis = glm::normalize(glm::vec3(-stat.acc.x, -stat.acc.y, -stat.acc.z));
            chuk_yaxis = glm::normalize(glm::vec3(-stat.nunchuck.acc.x, -stat.nunchuck.acc.y, -stat.nunchuck.acc.z));
        }

    /*  Calculate matrices to rotate the model's "up" to match the Wii Remote's "up" from before.
     *  First: cosine of the rotation angle. */
        float cosRot = glm::dot(glm::vec3(0, 1, 0), mote_yaxis);
    /*  Then, the axis of rotation. */
        glm::vec3 rotAxis = glm::cross(glm::vec3(0, 1, 0), mote_yaxis);
    /*  Create a basic matrix to rotate; starting off with a translation to world coordinates (2,0,0) */
        glm::mat4 mote_ModelMatrix = glm::translate(glm::mat4(1), glm::vec3(2, 0, 0));
    /*  Special case: input vectors were parallel. */
        if (rotAxis == glm::vec3(0, 0, 0)) {
        /*  Negative cos -> vectors were in opposite directions. Therefore, remote is upside down */
            if (cosRot < 0) {
                mote_ModelMatrix = glm::rotate(mote_ModelMatrix,
                    glm::radians(180.0f), glm::vec3(0, 0, 1)
                );
            }
        /*  Positive cos means wii remote is face-up, and no rotation is needed */
        } else {
        /*  Otherwise, do the rotation with the angle and axis as calculated */
            mote_ModelMatrix = glm::rotate(mote_ModelMatrix,
                glm::acos(cosRot), rotAxis
            );
        }
    /*  Calculate MVP matrix for shader to multiply out */
        glm::mat4 mote_MVPMatrix = ProjectionMatrix * ViewMatrix * mote_ModelMatrix;

    /*  Calculate matrices for Nunchuk */
        cosRot = glm::dot(glm::vec3(0, 1, 0), chuk_yaxis);
        rotAxis = glm::cross(glm::vec3(0, 1, 0), chuk_yaxis);
        glm::mat4 chuk_ModelMatrix = glm::translate(glm::mat4(1), glm::vec3(-2, 0, 0));
        if (rotAxis == glm::vec3(0, 0, 0)) {
            if (cosRot < 0) {
                chuk_ModelMatrix = glm::rotate(chuk_ModelMatrix,
                    glm::radians(180.0f), glm::vec3(0, 0, 1)
                );
            }
        } else {
            chuk_ModelMatrix = glm::rotate(chuk_ModelMatrix,
                glm::acos(cosRot), rotAxis
            );
        }
        glm::mat4 chuk_MVPMatrix = ProjectionMatrix * ViewMatrix * chuk_ModelMatrix;

    /*  Begin rendering to the DRC */
        WHBGfxBeginRender();
        WHBGfxBeginRenderDRC();

        WHBGfxClearColor(0.0f, 0.5f, 0.7f, 1.0f);
    /*  Set position and colour vertex buffers */
        GX2RSetAttributeBuffer(&aPositions, aPosition, aPositions.elemSize, 0);
        GX2RSetAttributeBuffer(&aColours, aColour, aColours.elemSize, 0);
    /*  Set shaders */
        GX2SetFetchShader(&shader.fetchShader);
        GX2SetVertexShader(shader.vertexShader);
        GX2SetPixelShader(shader.pixelShader);

    /*  Set the Wii Remote's MVP matrix and render */
        GX2SetVertexUniformRegGLM(uMVP->offset, mote_MVPMatrix);
        GX2DrawIndexedImmediateEx(GX2_PRIMITIVE_MODE_QUADS, ARR_SIZE(indexes), GX2_INDEX_TYPE_U16, indexes, 0, 1);

    /*  Set the Nunchuk's MVP matrix and render the same model again */
        GX2SetVertexUniformRegGLM(uMVP->offset, chuk_MVPMatrix);
        GX2DrawIndexedImmediateEx(GX2_PRIMITIVE_MODE_QUADS, ARR_SIZE(indexes), GX2_INDEX_TYPE_U16, indexes, 0, 1);

    /*  Finish up render and commit image */
        WHBGfxFinishRenderDRC();
        WHBGfxFinishRender();
    }

    GX2RDestroyBufferEx(&aPositions, (GX2RResourceFlags)0);

    WHBGfxShutdown();
    WHBLogUdpDeinit();
    WHBProcShutdown();
    return 0;
}
